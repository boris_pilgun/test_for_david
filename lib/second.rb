
#separated method for calculating Luhn digit of source number
def calculate_luhn_digit(input_num)
  input_num = input_num.to_s.scan(/\d/).map(&:to_i).reverse

  #separate values at odd & even indexes
  even_numbers = input_num.values_at(* input_num.each_index.select{|k| k.even?})
  odd_numbers = input_num.values_at(* input_num.each_index.select{|k| k.odd?})

  #calculate sum
  result = odd_numbers.reduce{|sum, new_val| sum + new_val }
  even_numbers.each{|num| num *= 2; result += (num > 9 ? num - 9 : num)}

  #additional digit
  result = 10 - (result % 10)
  result == 10 ? 0 : result
end


#check if source number is valid
def number_is_valid?(number)
  number = number.to_s
  luhn_result = calculate_luhn_digit(number[0...number.size-1])
  number[number.size-1].to_i == luhn_result
end


#append luhn digit to the end of source number
def append_luhn_digit(number)
  number = number.to_s
  "#{number}#{calculate_luhn_digit(number[0..number.size-1])}".to_i
end


#some tests:

puts "calculate_luhn_digit method:"
puts calculate_luhn_digit(7999999712)
puts calculate_luhn_digit(7992739871)


puts "number_is_valid? method:"
puts number_is_valid?(79999997124)
puts number_is_valid?(79999997129)

puts number_is_valid?(79927398712)
puts number_is_valid?(79927398713)

puts "append_luhn_digit? method:"
puts append_luhn_digit(7999999712)
puts append_luhn_digit(7992739871)
