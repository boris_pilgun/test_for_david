# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    line_item_id 1
    service_id 1
    balance 1.5
  end
end
