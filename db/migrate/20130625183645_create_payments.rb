class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :line_item_id, :null => false
      t.integer :service_id, :null => false
      t.float :balance, :dafault => 0

      t.timestamps
    end

    add_index :payments, :line_item_id

    #should be here but commented out for testing with parallel requests and clear tests result
    #add_index :payments, [:line_item_id, :service_id], :unique => true
  end
end
