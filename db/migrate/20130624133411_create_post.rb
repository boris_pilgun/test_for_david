class CreatePost < ActiveRecord::Migration
  def change
    create_table :post do |t|
      t.string :title

      t.timestamps
    end
  end
end
