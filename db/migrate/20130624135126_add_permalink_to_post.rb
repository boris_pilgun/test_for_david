class AddPermalinkToPost < ActiveRecord::Migration

  def change
    add_column :post, :permalink, :string

    add_index :post, :permalink, :unique => true
  end

  def migrate(direction)
    super
    if direction == :up
      Rails.application.eager_load!  #preload all code in production to access Post model
      Post.all.each do |post|
        post.update_attribute :permalink, "#{post.id} #{post.title}".parameterize
        puts post.permalink
      end
      puts "All permalinks updated!"
    end
  end

end