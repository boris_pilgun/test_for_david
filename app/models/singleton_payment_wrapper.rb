require 'singleton'
class SingletonPaymentWrapper
  include Singleton

  def initialize()
  end

  def find_or_create_payment(args)
    payment = nil
    Payment.transaction do
      payment = Payment.where(:line_item_id => args[:line_item_id], :service_id => args[:service_id]).first
      unless payment
        payment = Payment.create(:line_item_id => args[:line_item_id], :service_id => args[:service_id])
      end
    end
    payment
  end
end