class Post < ActiveRecord::Base
  attr_accessible :title

  set_table_name 'post'

  validates :title, :presence => true, :length => {:minimum => 2}

  #!!!!!!!!
  # all next code should be added after running migration with permalink field:

  #validates :permalink, :presence => true, :unless => Proc.new{ new_record? }
  #in additional to uniqueness validation with indexes in DB we add validation to model:
  #validates :permalink, :uniqueness => true, :allow_nil => true


  #after_create :set_permalink
  #
  #private
  #
  #def set_permalink
  #  self.update_attribute :permalink, "#{self.id} #{self.title}".parameterize
  #end
end
