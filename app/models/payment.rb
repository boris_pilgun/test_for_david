class Payment < ActiveRecord::Base
  attr_accessible :balance, :line_item_id, :service_id

  belongs_to :service

  #validates :line_item_id, :uniqueness => { :scope => :service_id }  #commented out for testing with parallel requests
  validates :line_item_id, :service_id, :presence => true    #validates for presence of required fields

  #simulates some updates in payment object with help of transaction and pessimistic DB locking
  def self.update_with_lock(payment, args)
    #using Locking::Pessimistic to prevent conflict because of update line by 2 processes at once
    Payment.transaction do
      payment.lock!
      payment.balance = payment.balance.to_f + 10  # + args[:new_amount]
      payment.save!
    end
    payment
  end

end
