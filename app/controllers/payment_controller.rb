class PaymentController < ApplicationController

  @@mutex = Mutex.new

  def payment_process
    payment = nil
    @@mutex.synchronize do
      payment = SingletonPaymentWrapper.instance.find_or_create_payment(params)
    end
    if payment
      payment = Payment.update_with_lock(payment, params)
      render text: "#{payment.id} - #{payment.balance}"
    else
      render text: "blank"
    end
  end
end
